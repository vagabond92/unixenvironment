#!/bin/bash
#
function copy_zsh_environment () {
    echo -e "Copying zsh environment..."
    [[ ! -d "zsh" ]] && mkdir zsh
    [[ -e "${HOME}/.zshrc" ]] && cp ${HOME}/.zshrc zsh/.zshrc
    if [[ -d "${HOME}/.oh-my-zsh" ]]; then
        cp -r ${HOME}/.oh-my-zsh/custom zsh
    fi
    echo -e "Done."
}

function copy_brew_environment () {
    echo -e "Copying homebrew environment..."
    [[ ! -d "brew" ]] && mkdir brew
    if which brew > /dev/null 2>&1; then
        brew leaves > brew/.brew-leaves
        brew list --cask > brew/.brew-casks
    fi
    echo -e "Done."
}

function copy_python_environments () {
    for dir in "${HOME}/.pyenvs/*"; do
        local envName="$(basename $dir)"
        echo -e "Copying python environment $envName..."
        [[ -d "${HOME}/Environment/pyenvs/${envName}" ]] || mkdir -p "${HOME}/Environment/pyenvs/${envName}"
        source $dir/bin/activate
        pip freeze > "${HOME}/Environment/pyenvs/${envName}/requirements.txt"
        deactivate
        unset envName
    done

}

###########################
# SYNC SCRIPT STARTS HERE #
###########################

copy_zsh_environment
[[ "$(uname)" == "Darwin" ]] && copy_brew_environment
copy_python_environments
