# *NIX Environment for Jason K. Ellis

Welcome to the *nix environment for Jason K. Ellis. This is a collection of useful scripts, aliases, and configuration files that make it easy to replicate my environment across different machines with ease.

## What this does

I have recently started experimenting with zshell as that is the new default shell for OSX Catalina. So far, I'm liking it, but I still spend some time on other *NIX machines, so I am going to maintain the bash scripts for a while.

I do most of my development and work on OSX, so natually I use homebrew. I keep a list of brew leaves and brew casks to make it easy to replicate.

I have recently transitioned to using VSCode as my primary text editor. I will be updating these scripts to handle automatically configuring VSCode as I learn more about how to do that.

Finally, I have a standard python virtual environment that I use as a default. I keep the requirements.txt file here so that I can get up and running quickly.

## How to use this repository

1. Clone the repository to the machine on which you want to install the environment.

    ```bash
    git clone git@bitbucket.org:vagabond92/unixenvironment.git Environment
    ```

2. Execute the install script.

    ```bash
    ./install.sh
    ```

3. To sync your current environment to the repository, execute the sync script, and then git commit/push as normal.

    ```bash
    ./sync.sh
    ```
