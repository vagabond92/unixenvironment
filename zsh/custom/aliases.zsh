#!/bin/bash

# enable color support of ls and also add handy aliases
if [[ "$(uname)" == "Linux" ]]; then
    if [ -x /usr/bin/dircolors ]; then
        test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
        alias ls="ls --color=auto"
        alias dir="vdir -h --color=auto"
        alias dir.="vdir -ah --color=auto"
        alias grep="grep --color=auto"
        alias fgrep="fgrep --color=auto"
        alias egrep="egrep --color=auto"
    fi
elif [[ "$(uname)" == "Darwin" ]]; then
    alias dir="ls -lh"
    alias dir.="ls -alh"
    if [ -n "$(which ggrep)" ]; then
        alias grep="grep --color=auto"
        alias fgrep="grep -F --color=auto"
        alias egrep="grep -E --color=auto"
    else
        alias grep="ggrep --color=auto"
        alias fgrep="ggrep -F --color=auto"
        alias egrep="ggrep -E --color=auto"
    fi
fi

# Aliases for programs
[[ "$(which sort >/dev/null 2>&1)" ]] && alias env="env | sort"

# Aliases for Home computers:

alias belknap="ssh -X -Y vagabond@192.168.128.92"
alias mira="ssh -X -Y vagabond@192.168.128.46"
