#!/bin/bash
#
# .bash_functions
# ~/.bashrc : General-Purpose Functions for interactive shells
#
#=============================================================================
function aws_start_mfa_session_with_role() {
    local AWS_ROLE_ARN="arn:aws:iam::182213838815:role/$1"
    local AWS_ROLE_SESSION_NAME="$1-session"
    local MFA_SERIAL="arn:aws:iam::182213838815:mfa/$2"
    local MFA_TOKEN="$3"

    local CREDENTIALS=$(aws sts assume-role --role-arn ${AWS_ROLE_ARN} --role-session-name ${AWS_ROLE_SESSION_NAME} --serial-number ${MFA_SERIAL} --token-code ${MFA_TOKEN})
    export ACCESS_KEY_ID="$(echo ${CREDENTIALS} | jq -r .Credentials.AccessKeyId)"
    export AWS_SECRET_ACCESS_KEY="$(echo ${CREDENTIALS} | jq -r .Credentials.SecretAccessKey)"
    export AWS_SESSION_TOKEN="$(echo ${CREDENTIALS} | jq -r .Credentials.SessionToken)"
}
#=============================================================================
function aws_sts_decode() {
    aws sts decode-authorization-message --encoded-message $1 --profile developer | jq .DecodedMessage | sed -e 's/\\\"/\"/g' | sed 's/^.\(.*\).$/\1/' | python -m json.tool
}
#=============================================================================
function rm_dsstore() {
    find . -name ".DS_Store" -type f -delete
}
function ami_login() {
    user=$1
    ip=$2
    key=$3
    if [ -z "$3" ]; then
        ssh -i "${HOME}/.ssh/DM_Demo.pem" "${user}@${ip}"
    else
        ssh -i "${HOME}/.ssh/${key}" "${user}@${ip}"
    fi
}
#=============================================================================
function upgrade_pip_leaves {
    pip install --upgrade pip
    for pkg in $(pip_leaves | awk -F '==' '{print $1}'); do
        pip install -U $pkg
    done
}
#=============================================================================
function pip_leaves {
    pip freeze
}
#=============================================================================
function install_apm_packages {
    if [[ -d "${HOME}/.atom/atom-packages" ]]; then
        for package in $(cat ~/.atom/atom_packages); do
            if [[ ! -d "${HOME}/.atom/packages/${package}" ]]; then
                apm install $package
            fi
        done
    else
        echo -e 'File atom-packages not found in ~/.atom/ '
    fi
}
#=============================================================================
function export_apm_packages {
    apm list | sed 's/├── //g' | sed 's/└── //g' | sed '1,/Community/d' | awk -F '@' '{print $1}'
}
#=============================================================================
function brew_caveats {
    brew info --json $(brew list) | jq -r '.[] | select(.caveats != null) | "\n\nName: \(.name)\nCaveats: \(.caveats)"'
}
#=============================================================================
function update_brew {
    brew update && brew upgrade
    brew upgrade --cask --greedy
}
#=============================================================================
function neo {
    # usage: neo IP.ADDRESS <SSH_KEY>
    if [ -z "$2" ]; then
        ssh -i "$HOME/.ssh/DM_Demo.pem" ubuntu@"$1"
    else
        ssh -i $HOME/.ssh/"$2" ubuntu@"$1"
    fi
}
#=============================================================================
function parse_git_branch {
    git branch 2>/dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1) /'
}
#=============================================================================
function strip { #Strips trailing whitespace from each line of a file
    perl -ple 's/[ \t]+$//' $1 >$1~
    rm $1
    mv $1~ $1
}
#=============================================================================
function error { # error function for interactive shell functions
    ## USAGE:
    ##       error MESSAGE
    ##
    ## print MESSAGE on stderr
    echo "ERROR in function '${FUNCNAME[1]}' : $1"
} 1>&2
#=============================================================================
function warn { # warn function for interactive shell functions
    ## USAGE:
    ##       warn MESSAGE
    ##
    ## print MESSAGE on stderr
    echo "WARNING in function '${FUNCNAME[1]}' : $1"
} 1>&2
#=============================================================================
function tsdate { # date in time-stamp format
    command date +${TSDATEFMT:-%Y-%m-%d.%H%M}
}
#=============================================================================
function strlen {
    expr length "$1"
}
#=============================================================================
function hide { # Hides files by prepending '.'
    for f in "$@"; do
        command mv -i "$f" ".$f"
    done
}
#=============================================================================

function unhide { # unhides list of files by removing prepended '.'
    for f in "$@"; do
        command mv -i ".$f" "$f"
    done
}
#=============================================================================
function repeat() { # repeat STRING COUNT [VAR] -- repeat STRING COUNT times
    local s=i=1
    while ((i++ <= $2)); do s+="$1"; done
    if [ "$3" ]; then
        eval "$3='$s'"
    else
        echo "$s"
    fi
}
#=============================================================================
