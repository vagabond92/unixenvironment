#!/usr/bin/env bash
################################################################################
# Created Date: Saturday October 22, 2022                                      #
# Author: Jason K. Ellis                                                       #
# -----                                                                        #
# Modified By: Jason K. Ellis                                                  #
# Last Modified: Monday October 31, 2022 - 11:40:56 am                         #
# -----                                                                        #
# Copyright (c) 2022, Jason K. Ellis                                           #
# -----                                                                        #
# License: MIT License                                                         #
# https://opensource.org/licenses/MIT                                          #
# -----                                                                        #
# CHANGELOG:                                                                   #
# Date   	By	Comments                                                       #
# --------	---	---------------------------------------------------------------#
################################################################################

#!/bin/zsh
function check_prerequisites() {
    echo -e "Checking pre-requisites..." &&
        prereqs="ruby curl git"

    missing_packages=""
    for prereq in $(echo ${prereqs}); do
        if ! which ${prereq} >/dev/null 2>&1; then
            missing_packages="${missing_packages} ${prereq}"
        fi
    done
    [[ -n ${missing_packages} ]] &&
        echo -e "Missing packages: ${missing_packages}" &&
        exit 1 ||
        (echo -e "Done." && return 0)
}

function install_zsh_environment() {
    echo -e "Installing zshell environment..."
    # Check that oh-my-zsh is installed, and if it's not, install it.
    [[ ! -d "${HOME}/.oh-my-zsh" ]] &&
        sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

    # Copy in the aliases, functions, themes, and plugins.
    cp zsh/.zshrc ${HOME}/.zshrc
    cp -r zsh/custom/* ${HOME}/.oh-my-zsh/custom/
    echo -e 'Done.'
    return 0
}

function list_includes_item {
    local list="$1"
    local item="$2"
    if [[ $list =~ (^|[[:space:]])"$item"($|[[:space:]]) ]]; then
        # yes, list include item
        result=0
    else
        result=1
    fi
    return $result
}

function remove_old_casks() {
    installed_casks=$(brew list --cask | tr "\n" " ")
    desired_casks=$(cat brew/.brew-casks | tr "\n" " ")
    for cask in $(echo ${installed_casks}); do
        if list_includes_item "${desired_casks}" "${cask}"; then
            true
        else
            echo -e "${cask} is installed and will be removed."
            brew uninstall --cask ${cask}
        fi
    done
}

remove_old_leaves() {
    installed_leaves=$(brew leaves | tr "\n" " ")
    desired_leaves=$(cat brew/.brew-leaves | tr "\n" " ")
    for leaf in $(echo ${installed_leaves}); do
        if list_includes_item "${desired_leaves}" "${leaf}"; then
            true
        else
            echo -e "${leaf} is installed and will be removed."
            brew uninstall ${leaf}
        fi
    done
    brew autoremove
}

install_casks() {
    echo -e "Installing casks..."
    for CASK in $(cat brew/.brew-casks); do
        if brew info --cask ${CASK} | grep 'Not installed' >/dev/null 2>&1; then
            brew install --cask ${CASK} --force
            echo -e "Installed ${CASK}."
        fi
    done
    echo -e "Done."
}

function install_leaves() {
    echo -e "Installing leaves..."
    for LEAF in $(cat brew/.brew-leaves); do
        if ! brew ls --versions ${LEAF} >/dev/null 2>&1; then
            brew install ${LEAF}
            echo -e "Installed ${LEAF}."
        fi
    done
    echo -e "Done."
}

function install_brew_packages() {
    echo -e "Installing homebrew formulae..." &&
        brew update &&
        brew upgrade &&
        install_casks &&
        remove_old_casks &&
        install_leaves &&
        remove_old_leaves &&
        brew upgrade &&
        brew upgrade --cask --greedy &&
        brew cleanup &&
        return 0 ||
        return 1
}

function check_brew() {
    echo -e "Checking for homebrew..."
    if which brew >/dev/null 2>&1; then
        brew update &&
            echo -e "Done." &&
            return 0 ||
            return 1
    else
        /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)" &&
            brew update &&
            echo -e "Done." &&
            return 0 ||
            return 1
    fi
}

function install_brew_environment() {
    echo -e "Installing homebrew environment..."
    check_brew &&
        install_brew_packages
    echo -e "Done." &&
        return 0 ||
        return 1
}

function install_python_envs() {
    [[ -d "${HOME}/.pyenvs" ]] || mkdir -p "${HOME}/.pyenvs"
    local start=$(pwd)
    cd "${HOME}/.pyenvs" && python3 -m venv normal && source "${HOME}/.pyenvs/normal/bin/activate"
    cd "${start}" && pip install --upgrade pip setuptools wheel && pip install -r pyenvs/normal/requirements.txt
    unset start

}

##############################
# INSTALL SCRIPT STARTS HERE #
##############################
check_prerequisites

[[ -e /bin/zsh ]] && install_zsh_environment ||
    (echo -e "Error installing zsh environment." && exit 1)

[[ "$(uname)" == "Darwin" ]] && install_brew_environment ||
    (echo -e "Error installing homebrew environment." && exit 1)

[[ $(which python3) ]] && install_python_envs ||
    (echo -e "Error installing python environments." && exit 1)
