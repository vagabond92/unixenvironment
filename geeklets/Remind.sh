#!/usr/bin/env bash
################################################################################
# Created Date: Thursday June 9, 2016                                          #
# Author: Jason K. Ellis                                                       #
# -----                                                                        #
# Modified By: Jason K. Ellis                                                  #
# Last Modified: Friday October 21, 2022 - 5:34:12 pm                          #
# -----                                                                        #
# Copyright (c) 2016 - 2022, Jason K. Ellis                                    #
# -----                                                                        #
# License: MIT License                                                         #
# https://opensource.org/licenses/MIT                                          #
# -----                                                                        #
# CHANGELOG:                                                                   #
# Date   	By	Comments                                                       #
# --------	---	---------------------------------------------------------------#
# 22-10-21	JKE	removed machine dependent code, added file header              #
################################################################################

osascript "/Users/vagabond/Google Drive/Scripts/Reminders.scpt"
