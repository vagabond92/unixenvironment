#!/usr/bin/env bash
################################################################################
# Created Date: Monday May 23, 2016                                            #
# Author: Jason K. Ellis                                                       #
# -----                                                                        #
# Modified By: Jason K. Ellis                                                  #
# Last Modified: Friday October 21, 2022 - 5:31:10 pm                          #
# -----                                                                        #
# Copyright (c) 2016 - 2022, Jason K. Ellis                                    #
# -----                                                                        #
# License: MIT License                                                         #
# https://opensource.org/licenses/MIT                                          #
# -----                                                                        #
# CHANGELOG:                                                                   #
# Date   	By	Comments                                                       #
# --------	---	---------------------------------------------------------------#
# 22-10-21	JKE	added file header                                              #
################################################################################

#Top 10 processes
echo "----------Top Processes----------"
ps -arcwwwxo "command pid %cpu %mem" | head -11
