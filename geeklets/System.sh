#!/usr/bin/env bash
################################################################################
# Created Date: Monday May 23, 2016                                            #
# Author: Jason K. Ellis                                                       #
# -----                                                                        #
# Modified By: Jason K. Ellis                                                  #
# Last Modified: Sunday October 23, 2022 - 11:47:48 am                         #
# -----                                                                        #
# Copyright (c) 2016 - 2022, Jason K. Ellis                                    #
# -----                                                                        #
# License: MIT License                                                         #
# https://opensource.org/licenses/MIT                                          #
# -----                                                                        #
# CHANGELOG:                                                                   #
# Date   	By	Comments                                                       #
# --------	---	---------------------------------------------------------------#
################################################################################
# 22-10-21	JKE - refactored to functions, added file header                   #
################################################################################

function uptime() {
    #Find time since last boot.
    boot=$(sysctl kern.boottime | awk '{print $5}' | sed "s/,//")
    now=$(date +%s)
    uptime=$(($now - $boot))

    #Convert time to human readable stuff.
    days=$(echo "$uptime / 86400" | bc)
    uptime=$(echo "$uptime - ($days * 86400)" | bc)
    hours=$(echo "$uptime / 3600" | bc)
    uptime=$(echo "$uptime - ( $hours * 3600)" | bc)
    minutes=$(($uptime / 60))
    #Current uptime
    echo ""
    echo "Uptime:     $days d $hours h $minutes m"
    echo ""
    unset boot
    unset now
    unset uptime
    unset days
    unset hours
    unset minutes
}

function cpu_usage() {
    #Current CPU usage

    # Get the number of cores
    [[ "$(uname)" = "Darwin" ]] && cores=$(sysctl -n hw.ncpu)
    [[ "$(uname)" = "linux" ]] && cores=$(nproc --all)

    # Calculate CPU usage
    myCPU=$(ps -axm -o %cpu | awk '{sum += $1} END {print sum}')
    myCPU=$(echo $myCPU / $cores | bc)

    # Create a nice usage bar
    declare -i b=9
    echo "CPU:       \c"
    while (($(echo "$b < $myCPU" | bc -l))); do
        echo "\033[1;31m*\033[0m\c"
        b=$(expr $b + 10)
    done
    [[ $myCPU -ne 100 ]] && echo "\033[1;37m*\033[0m\c"
    while [[ $b -lt 99 ]]; do
        echo "\033[1;37m*\033[0m\c"
        b=$(expr $b + 10)
    done
    echo " $myCPU%\c"
    echo "\r"
    unset b
    unset myCPU
    unset cores
}

function mem_usage() {
    #Current Memory Usage
    myUsedPer=$(ps -axm -o %mem | awk '{sum += $1} END {print sum}')
    myUsedPer=$(echo $myUsedPer / 1 | bc)
    # Create a nice usage bar
    declare -i c=9
    echo "Memory:    \c"
    while (($(echo "$c < $myUsedPer" | bc -l))); do
        echo "\033[1;31m*\033[0m\c"
        c=$(expr $c + 10)
    done
    [[ $myUsedPer -ne 100 ]] && echo "\033[1;37m*\033[0m\c"
    while [ $c -lt 99 ]; do
        echo "\033[1;37m*\033[0m\c"
        c=$(expr $c + 10)
    done
    echo " $myUsedPer%\c"

    echo "\r"

    unset c
    unset myUsedMem
    unset myFreeMem
    unset myTotalMem
    unset myUsedPer
}

function dsk_usage() {
    #Disk usage
    myDisk=$(df | awk '/dev\/disk1s1/' | grep -v com.apple | awk '{print $5}' | sed 's/\%//' | bc)

    declare -i a=9
    echo "Disk:      \c"
    while [[ "$a" -lt "$myDisk" ]]; do
        echo "\033[1;31m*\033[0m\c"
        a=$(expr $a + 10)
    done
    echo "\033[1;37m*\033[0m\c"
    while [ $a -lt 99 ]; do
        echo "\033[1;37m*\033[0m\c"
        a=$(expr $a + 10)
    done
    echo " $myDisk%\c"

    echo "\r"
    unset myDisk
    unset a
}

function bat_usage() {
    hasBattery="$(system_profiler SPPowerDataType | grep "Battery Information")"
    # Battery Power
    if [[ "${hasBattery}" ]]; then
        ac_adapt=$(ioreg -w0 -l | grep "ExternalConnected" | awk '{print $NF}')
        cur_power=$(ioreg -w0 -l | grep \"CurrentCapacity\" | awk '{print $NF}')
        max_power=$(ioreg -w0 -l | grep \"MaxCapacity\" | awk '{print $NF}')
        bat_percent=$(echo "scale=2;$cur_power / $max_power" | bc)
        bat_percent=$(echo "$bat_percent * 100" | bc | sed 's/\.00//')
        if [[ "$ac_adapt" != "" ]]; then
            declare -i a=9
            if [ "$ac_adapt" == "Yes" ]; then
                echo "Charging:  \c"
            else
                echo "Battery:   \c"
            fi

            while [ "$a" -lt "$bat_percent" ]; do
                echo "\033[1;31m*\033[0m\c"
                a=$(expr $a + 10)
            done
            [[ $bat_percent -ne 100 ]] && echo "\033[1;37m*\033[0m\c"
            while [ "$a" -lt 99 ]; do
                echo "\033[1;37m*\033[0m\c"
                a=$(expr $a + 10)
            done
            echo " $bat_percent%\c"
            unset bat_percent
            unset a
        fi
    fi
    unset hasBattery
}
### Script Starts Here ###
echo "----System Information---"
uptime
cpu_usage
mem_usage
dsk_usage
bat_usage
