#!/usr/bin/env bash
################################################################################
# Created Date: Monday May 23, 2016                                            #
# Author: Jason K. Ellis                                                       #
# -----                                                                        #
# Modified By: Jason K. Ellis                                                  #
# Last Modified: Friday October 21, 2022 - 5:30:01 pm                          #
# -----                                                                        #
# Copyright (c) 2016 - 2022, Jason K. Ellis                                    #
# -----                                                                        #
# License: MIT License                                                         #
# https://opensource.org/licenses/MIT                                          #
# -----                                                                        #
# CHANGELOG:                                                                   #
# Date   	By	Comments                                                       #
# --------	---	---------------------------------------------------------------#
# 22-10-21	JKE	removed hardline ethernet, added file header, generalized ip   #
#               address lookup, eliminated machine dependent code              #
################################################################################
# Color & Format
end="\x1b[0m"
red="\x1b[031m"
green="\x1b[032m"
yellow="\x1b[033m"

displayWirelessInfo() {
    wirelessIP=$(ifconfig -l | xargs -n1 ipconfig getifaddr)
    desiredWorkSSIDList=""
    desiredHomeSSIDList="Volcano-1 Volcano-2 Volcano-Guest"

    # Wireless Network SSID
    currentSSID=$(/System/Library/PrivateFrameworks/Apple80211.framework/Versions/A/Resources/airport -I | awk -F: '/ SSID: / {print $2}' | sed -e 's/SSID: //' | sed -e 's/ //')

    if [ -n "$currentSSID" ]; then
        myNetwork="false"
        for ssid in $desiredHomeSSIDList; do
            if [ "$ssid" = "$currentSSID" ]; then
                myNetwork="true"
                break
            fi
        done
        if [ "$myNetwork" = "false" ]; then
            for ssid in $desiredWorkSSIDList; do
                if [ "$ssid" = "$currentSSID" ]; then
                    myNetwork="true"
                    break
                fi
            done
        fi
        if [ "$myNetwork" = "false" ]; then
            echo "Wireless SSID:  ${yellow}$currentSSID${end}"
        else
            echo "Wireless SSID:  ${green}$currentSSID${end}"
        fi
        if [ -n "$wirelessIP" ]; then
            echo "Wireless IP:    ${green}$wirelessIP${end}"
        fi

    else
        echo "Wireless SSID:  ${red}"Not Connected"${end}"
    fi
}

displayExternalIp() {
    # External IP address
    staticIP="50.35.74.75"

    externalIP=$(curl ifconfig.me)

    myExternal="false"
    if [ -z "${externalIP}" ]; then
        echo "External IP:    ${red}No Internet!!${end}"
    elif [ -n "${externalIP}" ]; then
        [[ "$externalIP" == "$staticIP" ]] && myExternal="true"
    fi

    if [ "$myExternal" = "false" ]; then
        echo "External IP:    ${yellow}$externalIP${end}"
    else
        echo "External IP:    ${green}$externalIP${end}"
    fi
    return 0
}

#----SCRIPT STARTS----#
echo "----------Network Information----------\n"
displayWirelessInfo
displayExternalIp
